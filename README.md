# README #

### pst-unzipper ###

* Use PSTUnzipper python script to extract attachments and emails from a pst file

### Setup ###

* This script requires ***MS Outlook*** to perform the task of extraction.
      Use this script to recursively walk through the directory structure the email files and their attachments.
      The extraction maintains the same directory structure as in the pst file. 




* To run this script use

                      python PSTUnzipper -d "C:\Outlook Files\abc.pst" -o "C:\results\"



### Contribution guidelines ###

* Make sure your fork is up to date

                      git remote add ufediscovery https://bitbucket.org/ufediscovery/pst-unzipper/
                      
                      git pull ufediscovery master



* Create a branch

                      git checkout -b BRANCH_NAME




* To add and commit changes use:

                      git add pst-unzipper/test

                      git commit -am "YOUR_MSG"



* To push changes use:

                      git push origin BRANCH_NAME



### Who do I talk to? ###

* Repo owner or admin