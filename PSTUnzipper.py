#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""PSTUnzipper

This class is for extracting emails and attachments from PST files   

Created on: Tuesday, July 2 2014
Created by: Srinivas Balaji Ramesh

"""

__version__ = "0.1b" # beta version 
# $Source$

print __doc__ # prints script info.

import os
import win32com.client
import argparse
import sys


class PSTUnzipper:
    
    def __init__(self):
        try:
            self.ns = win32com.client.Dispatch('Outlook.Application').getNamespace('MAPI')
        except Exception, e:
            print 'MS Outlook application is required to run this program.'
            print e 
            sys.exit()
        

    def recursivelyParseFoldersAndExtract(self, parent, destinationPath, lastIdx):
        '''
        Recursively walks through the directory structure , extracts and indexes 
        the email files and their attachments.
        
        Arguments: 
            parent - pst root folder
            destinationPAth - destination path
            lastIdx - Index value assigned
        
        Returns: 
            lastIdx - last Index value assigned
        '''
        if len(parent.Folders) >= 0:
            if not os.path.exists(destinationPath): 
                os.makedirs(destinationPath)
            # Save the email items 
            for item in list(enumerate(parent.items)):
                lastIdx = lastIdx + 1
                filePath = os.path.join(destinationPath, str(item[0]) + '.email')
                item[1].SaveAs(filePath, 0)
                # Save the attachments                             
                for attachments in item[1].Attachments:
                    lastIdx = lastIdx + 1
                    attachments.SaveAsFile(os.path.join(destinationPath, str(item[0]) + ' - '
                                                                   + str(attachments.FileName)))                          
        for folders in parent.Folders:
            lastIdx = self.recursivelyParseFoldersAndExtract(folders, 
                                    os.path.join(destinationPath, str(folders)), lastIdx )
        
        return lastIdx
        


    def extract(self, path, dest, lastIdx = 0):
        '''
        Checks if the root folder exists in the outlook store and calls the
        recursive procedure to extract and index the files.
        
        Arguments: 
            path - source path
            dest - destination path
            lastIdx - Index value assigned
        
        Returns: 
            self.write - writer object
            lastIdx -  last Index value assigned
        '''
        self.ns.AddStore(path)
        for emailNamespaces in self.ns.Stores:
            rootFolder = emailNamespaces.GetRootFolder()
            destinationPath = os.path.join(dest, os.path.split(str(emailNamespaces.FilePath))[1].split('.')[0])
            getPath = os.path.split(str(emailNamespaces.FilePath))[0]
            if  getPath == os.path.split(path)[0]:
                if not os.path.exists(destinationPath):
                    os.makedirs(destinationPath)
                lastIdx = self.recursivelyParseFoldersAndExtract(rootFolder, destinationPath, lastIdx)
                emailNamespaces.Session.RemoveStore(rootFolder)
               

'''
                                                Testing class functionality. 
    -------------------------------------------------------------------------------------------------------
''' 



if __name__ == '__main__':
    
    arg_parser = argparse.ArgumentParser('''

    Extracts email documents and attachments from PST files  

    Example: 
          python PSTUnzipper -d "C:\Outlook Files\\abc.pst" -o "C:\\results\"

    ''')
    arg_parser.add_argument("-d", dest="doc_path", type=str, 
                            help="PST doc path", required=True)
    arg_parser.add_argument("-o", dest="output_folder", type=str, 
                            help="Output folder", required=True)
    args = arg_parser.parse_args()

    
    doc_path = os.path.normpath(args.doc_path)
    output_folder = os.path.normpath(args.output_folder)
    if not os.path.exists(doc_path):
        print 'incorrect document path'
        sys.exit()
    
    
    
    converter = PSTUnzipper()
    converter.extract(doc_path, output_folder)
     
     
     
          

               
          
          


                                   
